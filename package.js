Package.describe({
  name: 'howcloud:react-build',
  summary: "Compile JSX in .jsx files for react based meteor apps"
});

Package.registerBuildPlugin({
  name: 'howcloud:react-build:jsx',
  use: ['babel-compiler@5.8.24_1'],
  sources: [
    'compilejsx.js'
  ]
});

Package.on_use(function(api) {
	// We need the Babel helpers as a run-time dependency of the generated code. [see https://github.com/meteor/react-packages/blob/devel/packages/jsx/package.js]
	api.imply('babel-runtime@0.1.4');
	api.use('isobuild:compiler-plugin@1.0.0');
});