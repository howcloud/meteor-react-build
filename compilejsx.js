// From https://github.com/meteor/react-packages/blob/devel/packages/jsx/jsx-plugin.js

Plugin.registerCompiler({
  extensions: ['jsx'],
}, function () {
  return new BabelCompiler({
    react: true
  });
});